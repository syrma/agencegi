package agence;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

import agence.client.ClientService;
import agence.event.GestionnaireEvent;
import agence.gestionnaire.GestionnaireService;
import data__access.dao.DAOFactory;

public class Activator implements BundleActivator {

	private static BundleContext context;
	private ServiceRegistration serviceRegistration;
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		System.out.println("start agence");
		Activator.context = bundleContext;
		ServiceReference[] references = context.getServiceReferences(DAOFactory.class.getName(), null);
		System.out.println(references);
		if(references == null){
			System.out.println("Aucun service.");
			return;
		}
		DAOFactory factory = (DAOFactory) context.getService(references[0]);
		ClientImpl o = new ClientImpl(factory);
		GestionnaireImpl oo = new GestionnaireImpl(factory);
		serviceRegistration = context.registerService(ClientService.class.getName(), o, null);
		serviceRegistration = context.registerService(GestionnaireService.class.getName(), oo, null);
		serviceRegistration = context.registerService(GestionnaireEvent.class.getName(), oo, null);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
		if(serviceRegistration != null)
			serviceRegistration.unregister();
	}

}
