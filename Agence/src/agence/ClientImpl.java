package agence;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

import agence.client.ClientService;
import agence.exceptions.RegistrationError;
import agence.exceptions.ReservationError;
import agence.exceptions.TechnicalError;
import data__access.dao.ClientsDAO;
import data__access.dao.DAOException;
import data__access.dao.DAOFactory;
import data__access.dao.ReservationsDAO;
import data__access.dao.VoituresDAO;
import data__access.types.Client;
import data__access.types.Voiture;

class ClientImpl implements ClientService{

	DAOFactory factory;
	ReservationsDAO daoRes;
	ClientsDAO daoClients;
	VoituresDAO daoVoitures;

	PrintWriter out;
	ArrayList<String> erreurs;
	Client client;

	private static final String ALGO_CHIFFREMENT = "SHA-256";    		
	public ClientImpl(DAOFactory factory) throws IOException {
		super();
		this.factory = factory;
		daoRes = factory.getReservationsDAO();
		daoClients = factory.getClientsDAO();
		daoVoitures = factory.getVoituresDAO();
	//	out = new PrintWriter(new FileWriter("C:/Users/Radja/Projet Eclipse Kepler/osgi/ClientImpl.log",true),true);
		out = new PrintWriter(new FileWriter("/home/romaissa/Programmation et cours/workspace/Agence/src/agence/ClientImpl.log",true),true);
		out.println("\n --------------- \n \nLog for " + new java.util.Date());
		out.println("Référence du DAO : " + daoRes);
	}


	@Override
	public void reserver(int numImmatriculation, int id, int nombreJours,
			Date dateDebut)  throws ReservationError, TechnicalError{
		try{
			out.println("Réservation de la voiture : " + numImmatriculation + " par le client numéro " + id);
			daoRes.create(numImmatriculation, id, nombreJours, dateDebut);
			out.println("Opération effectuée.");			
		}catch(DAOException ex){
			out.println("Exception reçue : " + ex.getMessage());
			if(ex.getCause()==null)
				throw new ReservationError(ex);
			throw new TechnicalError("Une erreur a eu lieu lors de la réservation",ex);
		}
	}

	@Override
	public Voiture[] listeVoitures(Date date) throws TechnicalError{
		try{
			out.println("Récupération de la liste de voitures pour la date donnée...");
			return daoVoitures.liste(date);
		}catch(DAOException ex){
			ex.printStackTrace(out);
			throw new TechnicalError("Une erreur est survenue.",ex);
		}
	}

	@Override
	public void inscrire(String nom, String password, String confirmation, String numTelephone) throws RegistrationError, TechnicalError{
		out.println("Inscription d'un nouveau client.");
		erreurs = new ArrayList<String>();
		client = new Client();
		verifierIdentite(nom,numTelephone);
		verifierMdp(password,confirmation);
		if(erreurs.size()>0){
			for(String erreur : erreurs)
				out.println(erreur);
			throw new RegistrationError(erreurs.toArray(new String[0]));
		}
		try{
			daoClients.create(client);
		}catch(DAOException ex){
			out.println(ex.getMessage());
			if(ex.getCause()==null)
				throw new RegistrationError("Le client existe déjà.");
			throw new TechnicalError("Une erreur est survenue lors de l'inscription.",ex);
		}
	}

	@Override
	public int authentifier(String numTel, String password) throws TechnicalError {
		try{
			out.println("Authentification du numéro " +numTel +"...");
			Client client = daoClients.read(numTel);
			/* vérification du mot de passe */
			if(client == null){
				out.println("Client inexistant.");
				return -1;
			}
			ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
			passwordEncryptor.setAlgorithm(ALGO_CHIFFREMENT);
			passwordEncryptor.setPlainDigest(false);
			out.println("Vérification du mot de passe...");
			boolean auth = passwordEncryptor.checkPassword(password, client.getPassword());
			out.println(auth);
			return auth ? client.getId() : -1;
		}catch(DAOException ex){
			out.println(ex.getStackTrace());
			throw new TechnicalError("Une erreur est survenue.",ex);
		}
	}

	private void verifierIdentite(String nom, String numTel){
		out.printf("Vérification de l'identité '%s' '%s'\n", nom, numTel);
		if(nom==null || nom.trim().length()==0)
			erreurs.add("Veuillez entrer un nom.");
		
		client.setNom(nom);
		out.println(numTel.trim().length());
		if(numTel==null || (numTel.trim().length()!=10 && numTel.trim().length()!=9))
			erreurs.add("Veuillez entrer un numéro valide.");
		client.setNumTel(numTel);
	}

	private void verifierMdp(String mdp, String mdpConf){
		if(mdp== null || mdpConf==null){
			erreurs.add("Il faut remplir les deux champs de mot de passe.");
			return;
		}
		if(mdp.length() < 4)
			erreurs.add("Le mot de passe doit être constitué d'au moins 4 charactères.");
		else if(!mdp.equals(mdpConf))
			erreurs.add("Le mot de passe et la confirmation ne sont pas identiques.");
		else{
			ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
			passwordEncryptor.setAlgorithm(ALGO_CHIFFREMENT);
			passwordEncryptor.setPlainDigest(false);
			String mdpChiffre = passwordEncryptor.encryptPassword(mdp);
			client.setPassword(mdpChiffre); 
		}
	}
}
