package agence;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

import agence.event.GestionnaireEvent;
import agence.exceptions.InsertionError;
import agence.exceptions.SuppressionError;
import agence.exceptions.TechnicalError;
import agence.gestionnaire.CRListener;
import agence.gestionnaire.GestionnaireService;
import data__access.dao.ClientsDAO;
import data__access.dao.DAOException;
import data__access.dao.DAOFactory;
import data__access.dao.ReservationsDAO;
import data__access.dao.VoituresDAO;
import data__access.types.Client;
import data__access.types.Voiture;
public class GestionnaireImpl implements GestionnaireService, GestionnaireEvent {

	DAOFactory factory;
    ReservationsDAO daoReservation;
    ClientsDAO daoClients;
    VoituresDAO daoVoitures;
    
    PrintWriter out;
    ArrayList<CRListener> listeners;
    public GestionnaireImpl(DAOFactory factory) throws IOException {
    	super();
		this.factory = factory;
		daoReservation = factory.getReservationsDAO();
		daoClients = factory.getClientsDAO();
		daoVoitures = factory.getVoituresDAO();
		listeners = new ArrayList<CRListener>();
		
//		out = new PrintWriter(new FileWriter("C:/Users/Radja/Projet Eclipse Kepler/osgi/GestionnaireImpl.log",true),true);
		out = new PrintWriter(new FileWriter("/home/romaissa/Programmation et cours/workspace/Agence/src/agence//GestionnaireImpl.log",true),true);

		out.println("\n --------------- \n \nLog for " + new java.util.Date());
		out.println("Référence du DAO : " + daoReservation);
    }
    
    @Override
    public double recette(Date date) throws TechnicalError{
    	try{
    		out.println("Calcul de la recette de la date : " + date);
    		double rs = daoReservation.recette(date);
    		out.println("Opération effectuée : résultat reçu = " + rs);
    		return rs;
    	}catch(DAOException ex){
    		out.println(ex.getStackTrace());
    		throw new TechnicalError("Une erreur est survenue.",ex);
    	}
    }

	@Override
	public void bloquer(int id) throws TechnicalError {
		try{
			out.printf("Blocage du client %d...\n",id);
			daoClients.bloquer(id);
		}catch(DAOException ex){
			out.println(ex.getStackTrace());
			throw new TechnicalError(ex);
		}
	}

	@Override
	public void inserer(Voiture voiture) throws TechnicalError, InsertionError {
		try{
			out.printf("Insertion de la voiture %d...\n",voiture.getNumImmatriculation());
			daoVoitures.create(voiture);
			out.println("Insertion réussie.");
		}catch(DAOException ex){
			out.println(ex.getStackTrace());
			if(ex.getCause() ==null)
				throw new InsertionError(ex);
			throw new TechnicalError(ex);
		}
	}

	@Override
	public void supprimer(int numImmatriculation) throws TechnicalError,
			SuppressionError {
		try{
			out.printf("Suppression de la voiture %d...\n",numImmatriculation);
			daoVoitures.delete(numImmatriculation);
			out.println("Suppression réussie.");
		}catch(DAOException ex){
			out.println(ex.getStackTrace());
				throw new SuppressionError(ex);
			
		}
		
	}

	@Override
	public void addListener(CRListener listener) {
		listeners.add(listener);	
	}
	
	@Override
	public void removeListener(CRListener listener) {
		listeners.remove(listener);	
	}
	
	@Override
	public void fireEvent(){
		try{
			out.println("Appel de la liste.");
			Client[] retardataires = daoClients.liste();
			out.println("Nombre de retardataires : " + retardataires.length);
			if(retardataires.length ==0)
				return;
			for(CRListener listener : listeners)
				listener.scheduledAction(retardataires);
		}catch(DAOException ex){
			ex.printStackTrace(out);
		}
	}

	@Override
	public Voiture[] listeVoiture() throws TechnicalError {
		try{
			out.println("Récupération de la liste de toutes les voitures...");
			return daoVoitures.liste();
		}catch(DAOException ex){
			ex.printStackTrace(out);
			throw new TechnicalError("Une erreur est survenue.",ex);
		}
	}

	@Override
	public void liberer(int numImmatriculation) throws TechnicalError {
		try{
			out.printf("Libération de la voiture %d...\n",numImmatriculation);
			daoVoitures.liberer(numImmatriculation);
		}catch(DAOException ex){
			out.println(ex.getStackTrace());
			throw new TechnicalError(ex);
		}
	}
}