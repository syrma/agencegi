package agence.client;

import java.util.Date;

import agence.exceptions.RegistrationError;
import agence.exceptions.ReservationError;
import agence.exceptions.TechnicalError;
import data__access.types.Voiture;

//import data__access.dao.DAOException;
public interface ClientService {

	Voiture[] listeVoitures(Date date) throws TechnicalError;    
	void reserver(int numImmatriculation, int id, int nombreJours, Date dateDebut) throws ReservationError, TechnicalError;
    void inscrire(String nom, String password, String confirmation, String numTelephone) throws RegistrationError, TechnicalError;
    int authentifier(String numTel, String password) throws TechnicalError;
}
