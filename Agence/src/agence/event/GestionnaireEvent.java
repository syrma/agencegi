package agence.event;

import data__access.types.Client;

public interface GestionnaireEvent {
	void fireEvent();
}
