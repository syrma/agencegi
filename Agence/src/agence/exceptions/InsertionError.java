package agence.exceptions;

public class InsertionError extends Exception {

	private static final long serialVersionUID = 1L;

	public InsertionError(String message, Throwable cause) {
		super(message, cause);
	}

	public InsertionError(String message) {
		super(message);
	}

	public InsertionError(Throwable cause) {
		super(cause);
	}

}
