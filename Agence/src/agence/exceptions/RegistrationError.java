package agence.exceptions;

public class RegistrationError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String[] erreurs;
	
	public RegistrationError(String[] erreurs){
		this.erreurs = erreurs;
	}

	public RegistrationError(String message) {
		super(message);
	}
	
	@Override
	public String toString(){
		if(erreurs == null)
			return getMessage();
		
		String m = "";
		for(String erreur : erreurs)
			m+=erreur+"\n";
		return m;
	}
}
