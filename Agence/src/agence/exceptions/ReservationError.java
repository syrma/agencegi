package agence.exceptions;

public class ReservationError extends Exception {

	private static final long serialVersionUID = 1L;

	public ReservationError(String message, Throwable cause) {
		super(message, cause);
	}

	public ReservationError(String message) {
		super(message);
	}

	public ReservationError(Throwable cause) {
		super(cause);
	}
}
