package agence.exceptions;

public class SuppressionError extends Exception {

	private static final long serialVersionUID = 1L;

	public SuppressionError(String message, Throwable cause) {
		super(message, cause);
	}

	public SuppressionError(String message) {
		super(message);
	}

	public SuppressionError(Throwable cause) {
		super(cause);
	}

}
