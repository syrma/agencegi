package agence.exceptions;

public class TechnicalError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TechnicalError(String message, Throwable cause) {
		super(message, cause);
	}

	public TechnicalError(String message) {
		super(message);
	}

	public TechnicalError(Throwable cause) {
		super(cause);
	}

}
