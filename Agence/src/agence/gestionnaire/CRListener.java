package agence.gestionnaire;

import java.util.EventListener;

import data__access.types.Client;

public interface CRListener extends EventListener {

	void scheduledAction(Client[] retardataires);
}
