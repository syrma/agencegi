package agence.gestionnaire;

import java.util.Date;

import agence.exceptions.InsertionError;
import agence.exceptions.SuppressionError;
import agence.exceptions.TechnicalError;
import data__access.types.Voiture;

public interface GestionnaireService {

	Voiture[] listeVoiture() throws TechnicalError;
	void bloquer(int id) throws TechnicalError;
	double recette(Date date) throws TechnicalError;
	void inserer(Voiture voiture) throws TechnicalError, InsertionError;
	void supprimer(int numImmatriculation) throws TechnicalError, SuppressionError;
	void liberer(int numImmatriculation) throws TechnicalError;
	void addListener(CRListener listener);
	void removeListener(CRListener listener);
}
