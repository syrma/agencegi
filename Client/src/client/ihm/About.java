package client.ihm;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.border.EmptyBorder;

public class About extends JDialog implements ActionListener{
	public About(JFrame parent){
		super(parent, "À propos");
	//	super();
		JPanel pano = new JPanel(new BorderLayout());
		JButton cloclo = new JButton("Close");
		String message = "<html><center><b>Mini Projet <br> <br></b> Module :  <br> <b>Programmation Orientée Composants</b> <br><br>Par : <br>Kitouni Romaïssa & Djeghri Radja</center></html>";
		JLabel label = new JLabel(message);
		pano.add("South",cloclo);
		pano.add("Center",label);
		cloclo.addActionListener(this);
		pano.setBorder(new EmptyBorder(10,10,10,10));
		setContentPane(pano);
		setResizable(false);
		pack();
		setLocationRelativeTo(parent);
	}
	public void actionPerformed(ActionEvent e){
		setVisible(false);
	}
}

