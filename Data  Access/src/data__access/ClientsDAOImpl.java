package data__access;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import data__access.dao.ClientsDAO;
import data__access.dao.DAOException;
import data__access.types.Client;

public class ClientsDAOImpl implements ClientsDAO {

	
	final static String SQL_BLOQUER_CLIENT = "UPDATE Clients SET bloque = 1 WHERE id=:id";
    final static String SQL_BLOQUER_CLIENT2 = "DELETE FROM Reservations WHERE id=:id";
    
    final static String SQL_CLIENTS_RETARDATAIRES = "SELECT DISTINCT Clients.id, nom, numTel FROM Clients "
    		+ "INNER JOIN Reservations ON Clients.id = Reservations.id AND"
    		+ " CURRENT_DATE >= DATE_ADD(Reservations.dateReservation, INTERVAL nombreJoursReservation-1 DAY)"
    		+ " WHERE bloque = 0;";
    
    final static String SQL_INSCRIRE_CLIENT = "INSERT INTO Clients(nom,password,numTel,bloque) "
    		+ "VALUES(:nom,:password,:numTel,0);";
    
    final static String SQL_AUTHENTIFIER = "SELECT * FROM Clients WHERE numTel=:numTel AND bloque = 0;";
    
    PrintWriter out;
    DAOFactoryImpl factory;
    
    public ClientsDAOImpl(DAOFactoryImpl factory) throws IOException{
    	this.factory = factory;
//    	out = new PrintWriter(new FileWriter("C:/Users/Radja/Projet Eclipse Kepler/osgi/ClientsDAOImpl.log",true),true);
    	out = new PrintWriter(new FileWriter("/home/romaissa/Programmation et cours/workspace/Data  Access/src/data__access/ClientsDAOImpl.log",true),true);    	
		out.println("\n --------------- \n \nLog for " + new java.util.Date());
    }
    
	@Override
	public Client[] liste() throws DAOException {
		out.println("Récupération de la liste des clients retardataires...");
		Statement stmt = null;
		ResultSet rs = null;
		try(Connection cnx = factory.getConnection()){
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_CLIENTS_RETARDATAIRES);
			if(!rs.next())
				return new Client[0];
			return map(rs);
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}

	@Override
	public void bloquer(int id) throws DAOException {
		out.println("Lancement de l'opération pour bloquer un client...");
		NamedParameterStatement stmt = null;
		try(Connection cnx = factory.getConnection()){
			cnx.setAutoCommit(false);
			stmt = new NamedParameterStatement(cnx,SQL_BLOQUER_CLIENT);
			stmt.setInt("id", id);
			out.println("SQL à exécuter : \n" + stmt);
			int i = stmt.executeUpdate();
			out.println("Nombre d'opérations : " + i);
			stmt = new NamedParameterStatement(cnx,SQL_BLOQUER_CLIENT2);
			stmt.setInt("id", id);
			out.println("SQL à exécuter : \n" + stmt);
			int j = stmt.executeUpdate();
			out.println("Nombre d'opérations : " + j);
			if(i == 0 || j == 0)
				throw new DAOException("Impossible de bloquer ce client.");
			cnx.commit();
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}

	}

	@Override
	public Client read(String numTel) throws DAOException,
	IllegalArgumentException {
		out.println("Recherche d'un client...");
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_AUTHENTIFIER);
			stmt.setString("numTel", numTel);
			out.println("SQL à exécuter : \n" + stmt);
			rs = stmt.executeQuery();
			if(!rs.next())
				return null;
			return mapSingle(rs);
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}
	
	@Override
	public void create(Client client) throws DAOException,IllegalArgumentException{
		out.println("Création d'un nouveau client...");
		NamedParameterStatement stmt = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_INSCRIRE_CLIENT);
			stmt.setString("nom", client.getNom());
			stmt.setString("password", client.getPassword());
			stmt.setString("numTel", client.getNumTel());
			out.println("SQL à exécuter : \n" + stmt);
			int i = stmt.executeUpdate();
			out.println("Nombre d'ajouts : " + i);
			if(i == 0)
				throw new DAOException("Impossible de créer un client.");
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
		
	}
	
	private Client[] map(ResultSet rs) throws SQLException{		
		ArrayList<Client> clients = new ArrayList<Client>();
		do{
		int id = rs.getInt("id");
		String nom = rs.getString("nom");
		String numTel = rs.getString("numTel");
		clients.add(new Client(id,nom,numTel));
		}while(rs.next());
		return clients.toArray(new Client[0]);
	}

	private Client mapSingle(ResultSet rs) throws SQLException{		
		Client client = new Client();
		client.setId(rs.getInt("id"));
		client.setNom(rs.getString("nom"));
		client.setNumTel(rs.getString("numTel"));
		client.setPassword(rs.getString("password"));
		return client;
	}
	
	

}
