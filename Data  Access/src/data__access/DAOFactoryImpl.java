package data__access;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import data__access.dao.ClientsDAO;
import data__access.dao.DAOFactory;
import data__access.dao.ReservationsDAO;
import data__access.dao.VoituresDAO;

public class DAOFactoryImpl implements DAOFactory {
	private static final String FICHIER_PROPERTIES = "/data__access/dao.properties";
	private static final String PROPERTY_URL = "url";
	private static final String PROPERTY_DRIVER = "driver";
	private static final String PROPERTY_USERNAME = "username";
	private static final String PROPERTY_PASSWORD = "password";

	private String url;
	private String username;
	private String password;

	PrintWriter out;
	
	DAOFactoryImpl(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}

	public static DAOFactory getInstance(){
		String url, driver, username, password; 
		Properties properties = new Properties();

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream fichierp = loader.getResourceAsStream(FICHIER_PROPERTIES);
		if(fichierp==null)
			throw new DAOConfigurationException("Le fichier " + FICHIER_PROPERTIES + " est introuvable.");

		try{
			properties.load(fichierp);
			url = properties.getProperty(PROPERTY_URL);
			driver = properties.getProperty(PROPERTY_DRIVER);
			username = properties.getProperty(PROPERTY_USERNAME);
			password = properties.getProperty(PROPERTY_PASSWORD);
		}catch(IOException e){
			throw new DAOConfigurationException("Impossible de charger le fichier de configuration.",e);
		}

		try{
			Class.forName(driver);
		}catch(ClassNotFoundException e){
			throw new DAOConfigurationException("Impossible de charger le driver.",e);    		
		}

		return new DAOFactoryImpl(url,username,password);
	}
	public Connection getConnection() throws SQLException{
		return DriverManager.getConnection(url,username,password);
	}

	@Override
	public ClientsDAO getClientsDAO() throws IOException {
		return new ClientsDAOImpl(this);
	}

	@Override
	public ReservationsDAO getReservationsDAO() throws IOException {
		return new ReservationsDAOImpl(this);
	}

	@Override
	public VoituresDAO getVoituresDAO() throws IOException {
		return new VoituresDAOImpl(this);
	}

}
