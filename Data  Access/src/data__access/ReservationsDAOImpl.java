package data__access;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import data__access.dao.DAOException;
import data__access.dao.ReservationsDAO;

public class ReservationsDAOImpl implements ReservationsDAO {

	DAOFactoryImpl factory ;
	PrintWriter out;
	
    final static String SQL_RESERVER_VOITURE = "INSERT INTO Reservations SELECT"
    	+ " :numImmatriculation,:id,:dateReservation,:nombreJoursReservation FROM"
    	+ " dual WHERE NOT EXISTS (SELECT * from Reservations WHERE "
    	+ "numImmatriculation = :numImmatriculation AND "
    	+ "((:dateReservation >= dateReservation AND"
    	+ " :dateReservation < DATE_ADD(dateReservation, INTERVAL nombreJoursReservation DAY))"
    	+ " OR dateReservation BETWEEN :dateReservation AND "
    	+ "DATE_ADD(:dateReservation,INTERVAL :nombreJoursReservation-1 DAY)));";
    final static String SQL_RECETTE = "SELECT SUM(tarif * nombreJoursReservation) FROM Voitures INNER JOIN Reservations ON Voitures.numImmatriculation = Reservations.numImmatriculation AND dateReservation = :dateReservation;";

    final static String SQL_LIBERER_VOITURE = "DELETE from Reservations "
    		+ "WHERE numImmatriculation = :numImmatriculation AND dateReservation <= CURRENT_DATE ; ";
    
	public ReservationsDAOImpl(DAOFactoryImpl factory) throws IOException {
		super();
		this.factory = factory;
//		out = new PrintWriter(new FileWriter("C:/Users/Radja/Projet Eclipse Kepler/osgi/ReservationsDAOImpl.log",true),true);
		out = new PrintWriter(new FileWriter("/home/romaissa/Programmation et cours/workspace/Data  Access/src/data__access/ReservationsDAOImpl.log",true),true);
		out.println("\n --------------- \n \nLog for " + new java.util.Date());
	}
/*
 * (non-Javadoc)
 * @see data__access.dao.ReservationsDAO#create(int, int, int, java.util.Date)
 * numImmatriculation, id, dateReservation, nombreJoursReservation
 */
	@Override
	public void create(int numImmatriculation, int id, int nombreJours,
			Date dateDebut) throws DAOException {
		out.println("Création d'une nouvelle réservation...");
		NamedParameterStatement stmt = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_RESERVER_VOITURE);
			stmt.setInt("numImmatriculation", numImmatriculation);
			stmt.setInt("id", id);
			stmt.setInt("nombreJoursReservation",nombreJours);
			stmt.setDate("dateReservation", new java.sql.Date(dateDebut.getTime()));
			out.println("SQL à exécuter : \n" + stmt);
			int i = stmt.executeUpdate();
			out.println("Nombre d'ajouts : " + i);
			if(i == 0)
				throw new DAOException("Impossible d'effectuer une réservation à ce jour.");
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}

	@Override
	public double recette(Date date) throws DAOException {
		out.println("Calcul de la recette de la date : " + date + "...");
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_RECETTE);
			stmt.setDate("dateReservation", new java.sql.Date(date.getTime()));
			rs = stmt.executeQuery();
			if(!rs.next())
				throw new DAOException("Erreur lors du calcul de la recette");
			double res = rs.getDouble(1);
			out.println("rs.getDouble(1) = " + res);
			return res;
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}
	@Override
	public void delete(int numImmatriculation) throws DAOException,
			IllegalArgumentException {
		out.println("Suppression des réservations de la voiture : " + numImmatriculation +"...");
		NamedParameterStatement stmt = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_LIBERER_VOITURE);
			stmt.setInt("numImmatriculation", numImmatriculation);
			out.println("SQL à exécuter : \n" + stmt);
			int i = stmt.executeUpdate();
			out.println("Nombre d'opérations : " + i);
			if(i == 0)
				throw new DAOException("Impossible de libérer cette voiture.");
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}	
	}
	

}
