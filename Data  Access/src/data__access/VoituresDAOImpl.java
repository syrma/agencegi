package data__access;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import data__access.dao.DAOException;
import data__access.dao.VoituresDAO;
import data__access.types.Voiture;


public class VoituresDAOImpl implements VoituresDAO {

	static final String SQL_CONSULTER_LISTE_VOITURE = "SELECT DISTINCT Voitures.numImmatriculation, marque,"
			+ " modele, tarif FROM Voitures LEFT JOIN Reservations ON"
			+ " Voitures.numImmatriculation = Reservations.numImmatriculation "
			+ "WHERE dateReservation IS NULL OR NOT EXISTS (SELECT * FROM Reservations "
			+ "WHERE (:Date >= dateReservation AND :Date < DATE_ADD(dateReservation, "
			+ "INTERVAL nombreJoursReservation DAY))) ;";
	
	static final String SQL_CONSULTER_LISTE_VOITURE2 = "SELECT Voitures.numImmatriculation,marque,modele,tarif,"
			+ " MAX(CURRENT_DATE BETWEEN dateReservation AND DATE_ADD(dateReservation, "
			+ "INTERVAL nombreJoursReservation-1 DAY)) as 'etat' FROM Voitures "
			+ "LEFT JOIN Reservations ON Voitures.numImmatriculation = Reservations.numImmatriculation "
			+ "GROUP BY Voitures.numImmatriculation;";

	static final String SQL_INSERER_VOITURE = "INSERT INTO Voitures "
			+ "VALUES(:numImmatriculation,:marque,:modele,:tarif);";
	
	static final String SQL_SUPPRIMER_VOITURE = "DELETE FROM Voitures "
			+ "WHERE numImmatriculation = :numImmatriculation";

	static final String SQL_LIBERER_VOITURE = "DELETE from Reservations "
			+ "WHERE numImmatriculation = :numImmatriculation "
			+ "AND dateReservation <= CURRENT_DATE ; ";
	PrintWriter out;
    DAOFactoryImpl factory;
    
    
	public VoituresDAOImpl(DAOFactoryImpl factory) throws IOException {
		super();
		this.factory = factory;
//		out = new PrintWriter(new FileWriter("C:/Users/Radja/Projet Eclipse Kepler/osgi/VoituresDAOImpl.log",true),true);
		out = new PrintWriter(new FileWriter("/home/romaissa/Programmation et cours/workspace/Data  Access/src/data__access/VoituresDAOImpl.log",true),true);
		out.println("\n --------------- \n \nLog for " + new java.util.Date());
	}

	@Override
	public Voiture[] liste(Date date) throws DAOException,
			IllegalArgumentException {
		out.println("Construction de la liste des voitures libres à la date : " + date +"...");
		NamedParameterStatement stmt = null;
		ResultSet rs = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_CONSULTER_LISTE_VOITURE);
			stmt.setDate("Date", new java.sql.Date(date.getTime()));
			out.println("SQL à exécuter : \n" + stmt);
			rs = stmt.executeQuery();
			if(!rs.next())
				throw new DAOException("Impossible de récupérer la liste des voitures.");
			return mapc(rs);
		}catch(SQLException ex){
			ex.printStackTrace(out);
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}

	@Override
	public Voiture[] liste() throws DAOException, IllegalArgumentException {
		out.println("Récupération de la liste de toutes les voitures...");
		Statement stmt = null;
		ResultSet rs = null;
		try(Connection cnx = factory.getConnection()){
			stmt = cnx.createStatement();
			rs = stmt.executeQuery(SQL_CONSULTER_LISTE_VOITURE2);
			if(!rs.next())
				throw new DAOException("Impossible de récupérer la liste des voitures.");
			return map(rs);
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}

	@Override
	public void create(Voiture voiture) throws DAOException,
			IllegalArgumentException {
		out.println("Insertion d'une nouvelle voiture...");
		NamedParameterStatement stmt = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_INSERER_VOITURE);
			stmt.setInt("numImmatriculation", voiture.getNumImmatriculation());
			stmt.setString("marque", voiture.getMarque());
			stmt.setString("modele", voiture.getModele());
			stmt.setDouble("tarif", voiture.getTarif());
			out.println("SQL à exécuter : \n" + stmt);
			int i = stmt.executeUpdate();
			out.println("Nombre d'ajouts : " + i);
			if(i == 0)
				throw new DAOException("Impossible d'insérer une voiture.");
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}

	}

	@Override
	public void delete(int numImmatriculation) throws DAOException,
			IllegalArgumentException {
		out.println("Suppression de la voiture : " + numImmatriculation +"...");
		NamedParameterStatement stmt = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_SUPPRIMER_VOITURE);
			stmt.setInt("numImmatriculation", numImmatriculation);
			out.println("SQL à exécuter : \n" + stmt);
			int i = stmt.executeUpdate();
			out.println("Nombre d'opérations : " + i);
			if(i == 0)
				throw new DAOException("Impossible de supprimer cette voiture.");
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}

	private Voiture[] mapc(ResultSet rs) throws SQLException{		
		ArrayList<Voiture> voitures = new ArrayList<Voiture>();
		do{
		int numImmatriculation = rs.getInt("numImmatriculation");
		String marque = rs.getString("marque");
		String modele = rs.getString("modele");
		double tarif = rs.getDouble("tarif");
		voitures.add(new Voiture(numImmatriculation,marque,modele,tarif));
		}while(rs.next());
		return voitures.toArray(new Voiture[0]);
	}
	
	private Voiture[] map(ResultSet rs) throws SQLException{		
		ArrayList<Voiture> voitures = new ArrayList<Voiture>();
		do{
		int numImmatriculation = rs.getInt("numImmatriculation");
		String marque = rs.getString("marque");
		String modele = rs.getString("modele");
		double tarif = rs.getDouble("tarif");
		boolean etat = rs.getBoolean("etat");
		voitures.add(new Voiture(numImmatriculation,marque,modele,tarif,etat));
		}while(rs.next());
		return voitures.toArray(new Voiture[0]);
	}

	@Override
	public void liberer(int numImmatriculation) throws DAOException,
			IllegalArgumentException {
		out.println("Libération de la voiture : " + numImmatriculation +"...");
		NamedParameterStatement stmt = null;
		try(Connection cnx = factory.getConnection();){
			stmt = new NamedParameterStatement(cnx,SQL_LIBERER_VOITURE);
			stmt.setInt("numImmatriculation", numImmatriculation);
			out.println("SQL à exécuter : \n" + stmt);
			int i = stmt.executeUpdate();
			out.println("Nombre d'opérations : " + i);
			if(i == 0)
				throw new DAOException("Impossible de libérer cette voiture.");
		}catch(SQLException ex){
			out.println(ex.getMessage());
			throw new DAOException(ex);
		}finally{
			try{
				stmt.close();
			}catch(Exception ex){
				out.println(ex.getMessage());
			}
		}
	}
}
