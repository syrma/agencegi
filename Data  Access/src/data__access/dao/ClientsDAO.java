package data__access.dao;
import data__access.types.Client;
public interface ClientsDAO {

	Client read(String numTel) throws DAOException,IllegalArgumentException;
	void create(Client client) throws DAOException, IllegalArgumentException;
	Client[] liste() throws DAOException, IllegalArgumentException;
	void bloquer(int id) throws DAOException, IllegalArgumentException;
}
