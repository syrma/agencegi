package data__access.dao;

import java.io.IOException;
import java.sql.Connection;

public interface DAOFactory {
	
	ClientsDAO getClientsDAO() throws IOException;
	ReservationsDAO getReservationsDAO() throws IOException;
	VoituresDAO getVoituresDAO() throws IOException;
}
