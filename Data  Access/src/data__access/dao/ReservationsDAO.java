package data__access.dao;

import java.util.Date;

public interface ReservationsDAO {

	void create(int numImmatriculation, int id, int nombreJours, Date dateDebut) throws DAOException, IllegalArgumentException;
	double recette(Date date) throws DAOException, IllegalArgumentException;
	void delete(int numImmatriculation) throws DAOException, IllegalArgumentException;
}
