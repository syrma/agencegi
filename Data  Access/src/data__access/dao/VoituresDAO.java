package data__access.dao;

import java.util.Date;

import data__access.types.Voiture;

public interface VoituresDAO {
	Voiture[] liste(Date date) throws DAOException, IllegalArgumentException;
	Voiture[] liste() throws DAOException, IllegalArgumentException;
	void create(Voiture voiture) throws DAOException, IllegalArgumentException;
	void delete(int numImmatriculation) throws DAOException, IllegalArgumentException;
	void liberer (int numImmatriculation) throws DAOException, IllegalArgumentException;
}
