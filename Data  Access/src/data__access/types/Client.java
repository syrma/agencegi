package data__access.types;

public class Client {
	private int id;
	private String nom, password, numTel;

	public Client(){
	}
	
	public Client(int id, String nom, String numTel){
		this.id = id; 
		this.nom = nom;
		this.numTel = numTel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString(){
		return String.format("%d \t \t %s \t \t %s",id, nom, numTel); 
	}
	
}
