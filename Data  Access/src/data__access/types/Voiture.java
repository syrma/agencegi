package data__access.types;

public class Voiture {

	private int numImmatriculation;
    private String marque;
    private String modele;
    private double tarif;
    private boolean etat;
    
	public Voiture(int numImmatriculation, String marque, String modele,
			double tarif, boolean etat) {
		super();
		this.numImmatriculation = numImmatriculation;
		this.marque = marque;
		this.modele = modele;
		this.tarif = tarif;
		this.etat = etat;
	}

	public Voiture(int numImmatriculation, String marque, String modele,
			double tarif) {
		super();
		this.numImmatriculation = numImmatriculation;
		this.marque = marque;
		this.modele = modele;
		this.tarif = tarif;
	}

	public int getNumImmatriculation() {
		return numImmatriculation;
	}

	public void setNumImmatriculation(int numImmatriculation) {
		this.numImmatriculation = numImmatriculation;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public double getTarif() {
		return tarif;
	}

	public void setTarif(double tarif) {
		this.tarif = tarif;
	}

	public boolean getEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	@Override
	public String toString() {
		return "Voiture [numImmatriculation=" + numImmatriculation
				+ ", marque=" + marque + ", modele=" + modele + ", tarif="
				+ tarif + ", etat=" + etat + "]";
	}
    
    
}
