package gestionnaire;

import gestionnaire.ihm.ApresAuthG;
import gestionnaire.ihm.ListeRetardataire;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import agence.gestionnaire.CRListener;
import agence.gestionnaire.GestionnaireService;
import data__access.types.Client;


public class Activator implements BundleActivator, CRListener {

	private static BundleContext context;
	private GestionnaireService proxy;
	private ApresAuthG fenetre;
	
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		System.out.println("start gestionnaire");
		ServiceReference[] references = context.getServiceReferences(GestionnaireService.class.getName(), null);
		if(references != null){
			proxy = (GestionnaireService) context.getService(references[0]);	
			proxy.addListener(this);
			fenetre = new ApresAuthG(proxy);
			fenetre.setVisible(true);
		}		
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		proxy.removeListener(this);
		Activator.context = null;
	}

	@Override
	public void scheduledAction(final Client[] retardataires) {
		System.out.println("Event reçu.");
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				int answer = JOptionPane.showConfirmDialog(fenetre, "Voulez-vous afficher la liste des clients retardataires ?", "Évenement clients retardataires", JOptionPane.YES_NO_OPTION);
				if(answer == JOptionPane.NO_OPTION)
					return;
				new ListeRetardataire(proxy, retardataires).setVisible(true);
			}
		});
		
	}
}
