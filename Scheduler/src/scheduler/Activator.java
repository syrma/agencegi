package scheduler;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.repeatMinutelyForever;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Calendar;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import agence.event.GestionnaireEvent;
public class Activator implements BundleActivator {

	static GestionnaireEvent proxy;
	Scheduler sched;
	
	static PrintWriter out;
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		System.out.println("start scheduler");
		ServiceReference[] references = bundleContext.getServiceReferences(GestionnaireEvent.class.getName(), null);
//		out = new PrintWriter(new FileWriter("C:/Users/Radja/Projet Eclipse Kepler/osgi/Activator.log",true),true);
		out = new PrintWriter(new FileWriter("/home/romaissa/Programmation et cours/workspace/Scheduler/src/scheduler/Activator.log",true),true);

		if(references!=null){
			proxy = bundleContext.getService(references[0]);
			out.println("First we must get a reference to a scheduler");
			SchedulerFactory sf = new StdSchedulerFactory();
			sched = sf.getScheduler();
			
			JobDetail job = newJob(RetardJob.class).withIdentity("job1", "group1").build();
			Calendar c = Calendar.getInstance();
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.HOUR_OF_DAY, 17);
			out.println("trigger");
			out.println(c.getTime());
			/*
			 * To repeat minutely

			Trigger trigger = newTrigger().withIdentity("trigger1", "group1")
					.startAt(c.getTime())
					.withSchedule(repeatMinutelyForever()).build();
			
			 */
			/*
			 * To repeat minutely
			 */

			Trigger trigger = newTrigger().withIdentity("trigger1", "group1")
					.startAt(c.getTime())
					.withSchedule(simpleSchedule().withIntervalInHours(24)).build();
			
			
			sched.scheduleJob(job, trigger);
			sched.start();
			out.println("scheduler started");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		sched.shutdown(true);
	}

}
