use RemotingAG;
delete from Reservations ;
delete from Clients ;
delete from Voitures ;
alter table Clients auto_increment = 1 ;

insert into Clients(nom,password,numTel,bloque) values('Ines','passExample','021212121',0),('Sarah','passExample','031313131',0),('Ramy','passExample','021314151',0),('Sabrina','passExample','031031031',0),('John','passExample','0777777777',0),('Eva','passExample','0555555555',0),('Yanis','passExample','0666666666',0),('Tom','passExample','051515151',0),('Lucas','passExample','0771772773',0),('Clara','passExample','0551552553',0),('Lila','passExample','021312131',0),('Marc','passExample','0123456789',0),('Maya','passExample','0987654321',0);
insert into Voitures values(123111525,'BMW','serie 7',9500),(123411425,'Seat','Lean',6500),(123511425,'Citroen','C4 Aircross',7500),(123611425,'Citroen','C4',7000),(123611525,'Chevrolet','Aveo',5000),(123711425,'Peugeot','207',6500),(123811325,'Kia','Picanto',5500),(123411525,'Kia','Picanto',5500),(12340016,'Kia','Sportage',8000),(12340018,'Renault','Clio',6500);
