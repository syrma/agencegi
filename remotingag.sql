USE RemotingAG;

DROP TABLE IF EXISTS Reservations,Voitures, Clients;

CREATE TABLE Voitures(
       numImmatriculation INT NOT NULL,
       marque VARCHAR(25) NOT NULL,
       modele VARCHAR(25) NOT NULL,
       tarif DOUBLE NOT NULL,
       PRIMARY KEY(numImmatriculation)
       )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;	

CREATE TABLE Clients(
       id INT NOT NULL auto_increment,
       nom VARCHAR(50) NOT NULL,
       password VARCHAR(56) NOT NULL,
       numTel VARCHAR(10) NOT NULL,
       bloque BOOLEAN NOT NULL,
       PRIMARY KEY(id),
       UNIQUE(numTel)
       )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;	

CREATE TABLE Reservations(
       numImmatriculation INT NOT NULL,
       id INT NOT NULL,
       dateReservation Date NOT NULL,
       nombreJoursReservation INT NOT NULL,
       PRIMARY KEY(numImmatriculation,dateReservation),
       CONSTRAINT voiture FOREIGN KEY(numImmatriculation) REFERENCES Voitures(numImmatriculation),
       CONSTRAINT client FOREIGN KEY(id) REFERENCES Clients(id)
       )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci ROW_FORMAT=DYNAMIC;	
